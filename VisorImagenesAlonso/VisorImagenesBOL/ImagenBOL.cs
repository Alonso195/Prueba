﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisorImagenesDAL;
using VisorImagenesENL;

namespace VisorImagenesBOL
{
    public class ImagenBOL
    {

        ImagenDAL idal = new ImagenDAL();

        public bool GuardarImagen(EImagen ima)
        {
            if (String.IsNullOrEmpty(ima.Titulo)
                || String.IsNullOrEmpty(ima.Tipo)
                || String.IsNullOrEmpty(ima.Descripcion))
            {
                throw new Exception("Datos de la imagen requeridos");
            }
            if (ima.Imagen == null)
            {
                throw new Exception("Imagen requerida");
            }
            return idal.Insertar(ima);
        }

        public List<EImagen> CargarImagenes()
        {
            return idal.CargarImagenes();
        }
    }
}
