﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisorImagenesENL;

namespace VisorImagenesDAL
{
    public class ImagenDAL
    {
        public bool Insertar(EImagen imagen)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"insert into imagen(imagen, titulo, tipo, descripcion) 
                            values (@imagen, @titulo, @tipo, @descripcion)";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                MemoryStream stream = new MemoryStream();
                imagen.Imagen.Save(stream, ImageFormat.Jpeg);
                byte[] pic = stream.ToArray();
                cmd.Parameters.AddWithValue("@imagen", pic);
                cmd.Parameters.AddWithValue("@titulo", imagen.Titulo);
                cmd.Parameters.AddWithValue("@tipo", imagen.Tipo);
                cmd.Parameters.AddWithValue("@descripcion", imagen.Descripcion);


                imagen.Id = Convert.ToInt32(cmd.ExecuteScalar());

                return cmd.ExecuteNonQuery() > 0;
            }
        }

        public List<EImagen> CargarImagenes()
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                List<EImagen> imagenes = new List<EImagen>();
                con.Open();
                string sql = @"select id, imagen, titulo, tipo, descripcion
	                            from imagen;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);


                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    imagenes.Add(CargarUsuario(reader));
                }

                return imagenes;
            }

            
        }

        private EImagen CargarUsuario(NpgsqlDataReader reader)
        {
            EImagen ima = new EImagen();
            //TODO: No es necesario validar el id, es solo un ejemplo
            ima.Id = reader["id"] != DBNull.Value ? Convert.ToInt32(reader["id"]) : 0;
            ima.Titulo = reader["titulo"].ToString();
            ima.Tipo = reader["tipo"].ToString();
            ima.Descripcion = reader["descripcion"].ToString();
            byte[] f = new byte[0];
            f = (byte[])reader["imagen"];
            MemoryStream stream = new MemoryStream(f);
            ima.Imagen = Image.FromStream(stream);
            return ima;
        }
    }
}
