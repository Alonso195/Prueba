﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisorImagenesBOL;
using VisorImagenesENL;

namespace VisorImagenesAlonso
{
    public partial class Form1 : Form
    {
        private EImagen ima;
        private ImagenBOL ibol;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ima = new EImagen();
            ibol = new ImagenBOL();
        }


        private void GuardarImagen_Click(object sender, EventArgs e)
        {
            try
            {
                ima.Titulo = txtTitulo.Text.Trim();
                ima.Tipo = txtTipo.Text.Trim();
                ima.Descripcion = txtDescripcion.Text.Trim();
                MessageBox.Show(ibol.GuardarImagen(ima) ? "Guardado" : "Error");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void CargarImagen()
        {
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                if (ima.Imagen == null)
                {
                      ima.Imagen = Image.FromFile(openFile.FileName);
                }
                else
                {
                     ima.Imagen = Image.FromFile(openFile.FileName);
                }
                 pbImagen.Image = ima.Imagen;
                
            }
        }

        private void SelectImagen_Click(object sender, EventArgs e)
        {
            CargarImagen();
        }

        private void CargarImagen_Click(object sender, EventArgs e)
        {
            dgvImagenes.DataSource = ibol.CargarImagenes();
        }

        private void ImagenPictureBox_Click(object sender, DataGridViewCellMouseEventArgs e)
        {
            int row = dgvImagenes.SelectedRows[0].Index;
           // pbImagen.Image =  dgvImagenes["imaCol", row].Valu;
            
        }
    }
}
