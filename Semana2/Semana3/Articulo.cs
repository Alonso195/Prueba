﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semana3
{
    class Articulo
    {
        public int Clave { get; set; }
        public string Descripcion { get; set; }
        public double Precio { get; set; }
        public int Cantidad { get; set; }

        internal double calcularIVA(double Precio)
        {
            return Precio * 0.13;
        }
    }
}
