﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semana3
{
    class Program
    {
        static void Main(string[] args)
        {
            // Ejercicio con Circunferencia
            Circunferencia moneda = new Circunferencia{Radio = 1.4};
            Circunferencia rueda = new Circunferencia { Radio = 10.2 };

            Console.WriteLine(moneda.CalcularArea());
            Console.WriteLine(moneda.CalcularPerimetro());
            Console.WriteLine(rueda.CalcularArea());
            Console.WriteLine(rueda.CalcularArea());
            Console.ReadKey();
            Console.Clear();

            //Ejercicio Rectángulo
            Rectangulo pared = new Rectangulo { Largo = 10, Ancho = 5 };
            Rectangulo ventana = new Rectangulo { Largo = 4, Ancho = 2 };

            double areaPared = pared.CalcularArea();
            double areaVentana = ventana.CalcularArea();
            double areaFinal = areaPared - areaVentana;

            Console.WriteLine("Area de la pared: " + areaPared);
            Console.WriteLine("Area de la ventana: " + areaVentana);
            Console.WriteLine("El tiempo para pintar es de " + pared.CalcularTiempo(areaFinal) + " minutos");
            Console.ReadKey();

            //Ejercicio con fechas
            Fecha fecha = new Fecha();
            Console.WriteLine(fecha.pasarMesString(8));
            Console.WriteLine(fecha.esBisiesto(2020));

            Console.ReadKey();
            Console.Clear();

            // Ejercicio de Articulos

            Articulo arroz = new Articulo { Clave = 1, Descripcion = "Arroz", Cantidad = 5, Precio = 1500 };
            Console.WriteLine("El IVA de {0} es {1}", arroz.Descripcion, arroz.calcularIVA(arroz.Precio));
            Console.ReadKey();
            Console.Clear();

            //Ejercicio de Temperatura
            Temperatura temperatura = new Temperatura { GradosCentigrados = 30 };
            Console.WriteLine("{0} grados centigrados son {1} grados farenheit", temperatura.GradosCentigrados,
                temperatura.ConvetirAFarenheit(temperatura.GradosCentigrados));
            Console.ReadKey();






        }
    }
}
