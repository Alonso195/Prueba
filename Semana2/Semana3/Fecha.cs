﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semana3
{
    class Fecha
    {
        private int day;
        private int month;
        private int year;

        public Fecha()
        {

        }

        public Fecha(int day, int month, int year)
        {
            this.day = day;
            this.month = month;
            this.year = year;

        }

        internal bool esBisiesto(int year)
        {
            if (year % 4 == 0 && year % 100 == 0 &&
                year % 800 == 0)
            {
                return true;
            }
            else if (year % 4 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        internal void verificarFecha()
        {
            if (year > 0)
            {
                if (month > 0 && month < 13)
                {
                    esBisiesto(year);
                }
            }
        }


        internal string pasarMesString(int mes)
        {
            switch (mes)
            {
                case 1:
                    return "Enero";
                    break;
                case 2:
                    return "Febrero";
                    break;
                case 3:
                    return "Marzo";
                    break;
                case 4:
                    return "Abril";
                    break;
                case 5:
                    return "Mayo";
                    break;
                case 6:
                    return "Junio";
                    break;
                case 7:
                    return "Julio";
                    break;
                case 8:
                    return "Agosto";
                    break;
                case 9:
                    return "Septiembre";
                    break;
                case 10:
                    return "Octubre";
                    break;
                case 11:
                    return "Noviembre";
                    break;
                case 12:
                    return "Diciembre";
                    break;

                default:
                    return "Fecha invalida";
                    break;
            }
        }





    }
}
