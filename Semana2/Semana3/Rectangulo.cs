﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semana3
{
    class Rectangulo
    {
        public double Largo { get; set; }
        public double Ancho { get; set; }

        internal double CalcularArea()
        {
            return Largo * Ancho;
        }

        internal double CalcularTiempo(double area)
        {
            return area * 10;
        }
    }
}
