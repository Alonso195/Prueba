﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semana3
{
    class Temperatura
    {
        public double GradosCentigrados { get; set; }

        internal double ConvetirAFarenheit(double GradosCentigrados)
        {
            return (GradosCentigrados * 1.8) + 32;
        }
    }
}
