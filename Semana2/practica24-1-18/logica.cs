﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica24_1_18
{
    class logica
    {
        public int MaximoComunDivisor(int num1, int num2)
        {
            int divisor = 2;

            int mcd = 1;

            while (num1 > divisor && num2 > divisor)
            {
                while (num1 % divisor == 0 && num2 % divisor == 0)
                {
                    num1 = num1 / divisor;
                    num2 = num2 / divisor;
                    mcd *= divisor;


                }
                divisor++;

            }

            return mcd;
        }

        public string NumeroPrimo(int numero)
        {
            int divisor = 2;
            while (numero > divisor)
            {
                if (numero % divisor == 0)
                {
                    return "No es un número primo";
                }
                divisor += 1;

            }
            return "Es un número primo";
        }

        public string NumeroPerfecto(int numero)
        {
            int divisor = 2;
            int cont = 0;
            while (numero >= divisor)
            {
                if (numero % divisor == 0)
                {
                    cont += numero / divisor;
                }
                divisor++;
            }
            if (numero == cont)
            {
                return "Es un número perfecto";
            }

            return "No es un número perfecto";

        }
    }
}
