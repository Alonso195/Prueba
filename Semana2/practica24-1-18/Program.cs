﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica24_1_18
{
    class Program
    {
        static int LeerInt(string texto)
        {
            int n1 = 0;
            do
            {
                Console.Write(texto + ": ");
            } while (!Int32.TryParse(Console.ReadLine(), out n1));
            return n1;
        }

        static void Main(string[] args)
        {
            logica log = new logica();
            

            while (true)

            {
                Console.Clear();
                Console.WriteLine("Ejercicio 1");
                int num1 = LeerInt("Digite el numero 1");
                int num2 = LeerInt("Digite el numero 2");
                Console.WriteLine("El MCD es: " + log.MaximoComunDivisor(num1, num2));
                Console.WriteLine("s: salir, otra para repetir");
                char res = Console.ReadKey().KeyChar;

                if (res == 's')
                {
                    break;
                }
                Console.ReadKey();
            }

            Console.Clear();
            
            string menu = "\nEjercicio2\n" +
               "1. Por meses\n" +
               "2. Por cantidad de conejos\n" +
               "3. Salir\n";

            bool salir = false;

            while (!salir)
            {
                Console.WriteLine(menu);
                int op = LeerInt("Que desea hacer");
                switch (op)
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        Console.WriteLine("Presione cualquier tecla para salir");
                        salir = true;
                        break;

                    default:
                        Console.WriteLine("Opción Inválida");
                        break;
                }
            }
            Console.Clear();

            while (true)
            {
                Console.WriteLine("Determinar si es un número primo");
                int numero = LeerInt("Digite un número");
                Console.WriteLine(log.NumeroPrimo(numero));
                Console.WriteLine("s: salir, otra para repetir");
                char res = Console.ReadKey().KeyChar;

                if (res == 's')
                {
                    break;
                }

            }

            while (true)
            {
                Console.Clear();
                Console.WriteLine("Determinar si es un número perfecto");
                int numero = LeerInt("Digite un número");
                Console.WriteLine(log.NumeroPerfecto(numero));
                Console.WriteLine("s: salir, otra para repetir");
                char res = Console.ReadKey().KeyChar;

                if (res == 's')
                {
                    break;
                }

            }

            Console.ReadKey();
        }
    }
}
