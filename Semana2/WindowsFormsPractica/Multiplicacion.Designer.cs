﻿namespace WindowsFormsPractica
{
    partial class Multiplicacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.operando1TextBox = new System.Windows.Forms.TextBox();
            this.operando2TextBox = new System.Windows.Forms.TextBox();
            this.multiplicarButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.resultadoTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.limpiarButton = new System.Windows.Forms.Button();
            this.SalirButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // operando1TextBox
            // 
            this.operando1TextBox.Location = new System.Drawing.Point(124, 27);
            this.operando1TextBox.Name = "operando1TextBox";
            this.operando1TextBox.Size = new System.Drawing.Size(100, 22);
            this.operando1TextBox.TabIndex = 0;
            // 
            // operando2TextBox
            // 
            this.operando2TextBox.Location = new System.Drawing.Point(124, 83);
            this.operando2TextBox.Name = "operando2TextBox";
            this.operando2TextBox.Size = new System.Drawing.Size(100, 22);
            this.operando2TextBox.TabIndex = 1;
            // 
            // multiplicarButton
            // 
            this.multiplicarButton.Location = new System.Drawing.Point(28, 207);
            this.multiplicarButton.Name = "multiplicarButton";
            this.multiplicarButton.Size = new System.Drawing.Size(100, 32);
            this.multiplicarButton.TabIndex = 2;
            this.multiplicarButton.Text = "Multiplicar";
            this.multiplicarButton.UseVisualStyleBackColor = true;
            this.multiplicarButton.Click += new System.EventHandler(this.multiplicarButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Operando 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Operando 2";
            // 
            // resultadoTextBox
            // 
            this.resultadoTextBox.Location = new System.Drawing.Point(124, 132);
            this.resultadoTextBox.Name = "resultadoTextBox";
            this.resultadoTextBox.Size = new System.Drawing.Size(100, 22);
            this.resultadoTextBox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Resultado";
            // 
            // limpiarButton
            // 
            this.limpiarButton.Location = new System.Drawing.Point(150, 207);
            this.limpiarButton.Name = "limpiarButton";
            this.limpiarButton.Size = new System.Drawing.Size(100, 32);
            this.limpiarButton.TabIndex = 7;
            this.limpiarButton.Text = "Limpiar";
            this.limpiarButton.UseVisualStyleBackColor = true;
            this.limpiarButton.Click += new System.EventHandler(this.limpiarButton_Click);
            // 
            // SalirButton
            // 
            this.SalirButton.Location = new System.Drawing.Point(277, 207);
            this.SalirButton.Name = "SalirButton";
            this.SalirButton.Size = new System.Drawing.Size(100, 32);
            this.SalirButton.TabIndex = 8;
            this.SalirButton.Text = "Salir";
            this.SalirButton.UseVisualStyleBackColor = true;
            this.SalirButton.Click += new System.EventHandler(this.SalirButton_Click);
            // 
            // Multiplicacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 322);
            this.Controls.Add(this.SalirButton);
            this.Controls.Add(this.limpiarButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.resultadoTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.multiplicarButton);
            this.Controls.Add(this.operando2TextBox);
            this.Controls.Add(this.operando1TextBox);
            this.Name = "Multiplicacion";
            this.Text = "Multiplica";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox operando1TextBox;
        private System.Windows.Forms.TextBox operando2TextBox;
        private System.Windows.Forms.Button multiplicarButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox resultadoTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button limpiarButton;
        private System.Windows.Forms.Button SalirButton;
    }
}

