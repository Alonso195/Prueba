﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsPractica
{
    public partial class Temperatura : Form
    {
        public Temperatura()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double gFarenheit;
            double gCentigrados;
            gCentigrados = Convert.ToDouble(tempEntradaTextBox.Text);
            gFarenheit = gCentigrados * 1.8 + 32.0;
            tempResultadoTextBox.Text = String.Format("{0:F3}", gFarenheit);
            


        }

        private void button1_Click(object sender, EventArgs e)
        {
            double gFarenheit;
            double gCentigrados;
            gFarenheit = Convert.ToDouble(tempEntradaTextBox.Text);
            gCentigrados = (gFarenheit - 32.0) / 1.8;
            tempResultadoTextBox.Text = String.Format("{0:F3}", gCentigrados);


        }

        private void button4_Click(object sender, EventArgs e)
        {
            tempEntradaTextBox.Text = "";
            tempResultadoTextBox.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
