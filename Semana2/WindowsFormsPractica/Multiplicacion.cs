﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsPractica
{
    public partial class Multiplicacion : Form
    {
        public Multiplicacion()
        {
            InitializeComponent();
        }

        private void multiplicarButton_Click(object sender, EventArgs e)
        {
            double num1;
            double num2;
            double resultado;
            string r;

            num1 = Convert.ToDouble(operando1TextBox.Text);
            num2 = Convert.ToDouble(operando2TextBox.Text);

            resultado = num1 * num2;
            r = String.Format("{0:F2}",resultado);

            resultadoTextBox.Text = r;


        }

        private void limpiarButton_Click(object sender, EventArgs e)
        {
            resultadoTextBox.Text = "";
            operando1TextBox.Text = "";
            operando2TextBox.Text = "";

        }

        private void SalirButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
