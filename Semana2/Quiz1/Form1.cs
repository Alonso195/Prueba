﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quiz1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Logica log = new Logica();

        private void button1_Click(object sender, EventArgs e) // obtiene el monto a pagar
        {
            double kilowats = Convert.ToDouble(kilowatstextBox.Text);
            double monto;
            monto = log.cuentaLuz(kilowats);
            montoPagarTextBox.Text = Convert.ToString(monto);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int edad = Convert.ToInt32(edadTextBox.Text);

            if (!"No es aceptado".Equals(log.obtenerUbicacionEdificio(edad)))
            {
                DialogResult result = System.Windows.Forms.MessageBox.Show("Le corresponde edificio "
                    + log.obtenerUbicacionEdificio(edad),
                    "Edificio", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DialogResult result = System.Windows.Forms.MessageBox.Show("No cumple con los requisitos de Edad",
                    "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            edadTextBox.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            double x = Convert.ToDouble(valorXTextBox.Text);
            double d = Convert.ToDouble(valorDTextBox.Text);

            DialogResult result = System.Windows.Forms.MessageBox.Show("El valor de D es: " + log.valoresPertinentesD(x, d),
                    "Valor D", MessageBoxButtons.OK, MessageBoxIcon.Information);
            valorXTextBox.Text = "";
            valorDTextBox.Text = "";

        }

        private void button4_Click(object sender, EventArgs e)
        {
            double gramos = Convert.ToDouble(gramosTextBox.Text);

            if (gramos < 200)
            {
                DialogResult result = System.Windows.Forms.MessageBox.Show("La tarifa es de: " + log.tarifaCorreo(gramos),
                   "Tarifa", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DialogResult result = System.Windows.Forms.MessageBox.Show("No puede enviar esta carta",
                   "Tarifa", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            double num1 = Convert.ToDouble(num1TextBox.Text);
            double num2 = Convert.ToDouble(num2TextBox.Text);

            DialogResult result = System.Windows.Forms.MessageBox.Show(log.verificarEstaBien(num1, num2),
                   "Esta Bien o Esta Mal", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button6_Click(object sender, EventArgs e)
        {

            for (int c = 0; c < 25; c++)
            {
                for (int b = 0; b < 24; b++)
                {
                    for (int a = 0; a < 23; a++)
                    {
                        if (a < b && b < c)
                        {

                            if (Math.Pow(a, 2) + Math.Pow(b, 2) == Math.Pow(c, 2))
                            {
                                listBox1.Text = Convert.ToString(a)+" + " + 
                                    Convert.ToString(b) + " = " + Convert.ToString(c);
                            }
                        }
                    }

                }
            }

        }
    }
}
