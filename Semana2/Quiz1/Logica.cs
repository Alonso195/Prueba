﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1
{
    class Logica
    {

        internal double cuentaLuz(double kilowatsH)
        {
            if (kilowatsH < 11)
            {
                return 10;
            }
            else if (kilowatsH > 11 && kilowatsH < 65)
            {
                return (kilowatsH - 11) * 2 + 10;
            }
            else
            {
                double mon1 = (kilowatsH - 11) * 2 + 10;
                return (kilowatsH - 65) * 4 + mon1;
            }
        }

        internal String obtenerUbicacionEdificio(int edad)
        {
            if (edad >= 16 && edad <= 18)
            {
                return "A";
            }

            else if (edad >= 19 && edad <= 20)
            {
                return "B";
            }

            else if (edad >= 21 && edad <= 25)
            {
                return "C";
            }
            else
            {
                return "No es aceptado";
            }
        }


        internal double valoresPertinentesD(double x, double d)
        {
            double porcentaje;
            if (d > 100)
            {
                if (x <= 1)
                {
                    return d + (d * 0.20);
                }
                if (x > 1 && x <= 2)
                {
                    return d + (d * 0.30);
                }
                if (x > 2 && x <= 3)
                {
                    return d + (d * 0.40);
                }
                else
                {
                    return d + (d * 0.50);
                }
            }
            else
            {
                return d;
            }
        }

        internal double tarifaCorreo(double gramos)
        {
            if (gramos < 19)
            {
                return 10;
            }
            else if (gramos >= 20 && gramos <= 30)
            {
                return (gramos - 20) * 2 + 10;
            }
            else
            {
                return (gramos - 30) * 1.50 + 30;
            }
        }

        internal string verificarEstaBien(double n1, double n2)
        {
            if (0 <= n1 && n1 <= n2 && n2 <= 100)
            {
                return "Esta Bien";
            }
            else
            {
                return "Esta Mal";
            }
        }

        internal void ternaPitagorica( )
        {
            for (int c = 0; c < 25; c++)
            {
                for (int b = 0; b < 24; b++)
                {
                    for (int a = 0; a < 23; a++)
                    {
                        if (a < b && b < c)
                        {

                            if (Math.Pow(a,2)+Math.Pow(b,2) == Math.Pow(c,2))
                            {

                            }
                        }
                    }

                }
            }
        }
    }
}
