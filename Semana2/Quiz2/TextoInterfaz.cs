﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quiz2
{
    public partial class TextoInterfaz : Form
    {
        TextoUtilidades textoUtilidades;
        public TextoInterfaz()
        {
            InitializeComponent();
        }

        private void TextoInterfaz_Load(object sender, EventArgs e)
        {
            textoUtilidades = new TextoUtilidades();
        }
        /// <summary>
        /// este evento llama al metodo EsPalindromo() de la clase textoUtilidades
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PalindromoButton1_Click(object sender, EventArgs e)
        {

            resultadotxb.Text = textoUtilidades.EsPalindromo(palindromoTextBox.Text);
            palindromoTextBox.Text = "";
        }

        /// <summary>
        /// Este evento llama al metodo subCadenaCaracteres
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubCadenaButton1_Click(object sender, EventArgs e)
        {
            int num1 = Convert.ToInt32(num1TextBox.Text);
            int num2 = Convert.ToInt32(num2TextBox.Text);
            string cadena = cadenaTextBox.Text;

            rtxtSubCadena.Text = textoUtilidades.subCadenaCaracteres(cadena,num1,num2);

        }
        /// <summary>
        /// Este evenyo llama al método MayúsculaCadena
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MayusculaButtonClick(object sender, EventArgs e)
        {
            string cadena = mayuculaTextBox.Text;
            DialogResult result = System.Windows.Forms.MessageBox.Show(textoUtilidades.MayusculaCadena(cadena).ToString(), 
                "Mayúsculas",MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Este evenyo llama al método CadenaValida en la clase TextoUtilidades
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValidarCadena(object sender, EventArgs e)
        {
            string cadena = validarCadenaTextBox.Text;
            string caracter = charTextBox.Text;
            DialogResult result = System.Windows.Forms.MessageBox.Show(textoUtilidades.CadenaValida(cadena,caracter),
               "Validar Cadena", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void CompararCadenaButtonClick(object sender, EventArgs e)
        {
            string cadena1 = cadena1TextBox.Text;
            string cadena2 = cadena2TextBox.Text;
            DialogResult result = System.Windows.Forms.MessageBox.Show(textoUtilidades.CompararCadena(cadena1,cadena2).ToString(),
              "Validar Cadena", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
