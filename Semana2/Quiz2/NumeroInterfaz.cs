﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quiz2
{
    public partial class NumeroInterfaz : Form
    {
        NumeroUtilidades numeroUtilidades;

        public NumeroInterfaz()
        {
            InitializeComponent();
        }


        private void NumeroInterfaz_Load(object sender, EventArgs e)
        {
            numeroUtilidades = new NumeroUtilidades();
        }

        /// <summary>
        /// Este envento llama al metodo NumeroNarcisista en la clase numeroUtilidades 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NumeroNarcisista(object sender, EventArgs e)
        {
            int num = Convert.ToInt32(narcisistaTextBox.Text);
            DialogResult result = System.Windows.Forms.MessageBox.Show(numeroUtilidades.NumeroNarcisista(num),
                "Número narcisista", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void RaizAproximadaButtonClick(object sender, EventArgs e)
        {
            int num = Convert.ToInt32(raizTextBox.Text);

            DialogResult result = System.Windows.Forms.MessageBox.Show(numeroUtilidades.RaizAproximada(num).ToString(),
                "Raíz aproximada", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void IntercambioValoresButtonClick(object sender, EventArgs e)
        {
            int a = Convert.ToInt32(textBox1.Text);
            int b = Convert.ToInt32(textBox2.Text);

            DialogResult result = System.Windows.Forms.MessageBox.Show(numeroUtilidades.IntercambioValores(a,b),
                "Intercambio valores de a y b", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int numero = Convert.ToInt32(textBox4.Text);

            DialogResult result = System.Windows.Forms.MessageBox.Show("Número invertido " + numeroUtilidades.Invertir(numero).ToString(),
                "Invertir número", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
