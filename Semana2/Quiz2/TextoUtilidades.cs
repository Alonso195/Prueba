﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2
{
    class TextoUtilidades
    {
        /// <summary>
        /// Este metodo invierte una palabra y varifica si es un palindromo
        /// </summary>
        /// <param name="palabra">parametro string ingresado por el usuario</param>
        /// <returns>retorna un string explicando el resultado</returns>
        internal string EsPalindromo(string palabra)
        {
            int tamPalabra = palabra.Length;
            string palabra2 = "";
            for (int i = tamPalabra - 1; i >= 0; i--)
            {
                palabra2 += palabra[i];
            }

            if (palabra.Equals(palabra2))
            {
                return String.Format("{0} es igual a ella misma invertida. \n" +
                    "Por lo tanto es un palíndromo", palabra);
            }
            else
            {
                return String.Format("{0} no es igual a {1} que es el resultado de invertirla.\n" +
                    "Por lo tanto no es un palíndromo ", palabra, palabra2);
            }

        }

        /// <summary>
        /// Dada una cadena de caracteres y dos números a y b,
        /// se obtiene una subcadena con los b caracteres de la primera cadena, a partir de a.
        /// </summary>
        /// <param name="cadena">Cadena de caracteres inicial</param>
        /// <param name="num1">posicion de inicio para la subcadena</param>
        /// <param name="num2">cantidad de caracteres que obtine la subcadena</param>
        /// <returns></returns>
        internal string subCadenaCaracteres(string cadena, int num1, int num2)
        {
            string subcadena = "";
            int cont = 0;

            for (int i = num1 - 1; i < cadena.Length; i++)
            {
                if (cont < num2)
                {
                    subcadena += cadena[i];
                    cont++;
                }
                else
                {
                    break;
                }
            }

            return subcadena;
        }

        /// <summary>
        /// Este método bool me permite saber si una cadena de carectares tiene todos su
        /// caracteres en mayúsculas
        /// </summary>
        /// <param name="cadena">cadena de caracteres que ingresa el usuario</param>
        /// <returns></returns>
        internal bool MayusculaCadena(string cadena)
        {
            string cadenaMayuscula = cadena.ToUpper();

            if (VerificarNumero(cadena))
            {
                return false;
            }
            else
            {
                if (cadena.Equals(cadenaMayuscula))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        /// <summary>
        /// Este método bool me permite saber si una cadena de carectares tiene todos su
        /// caracteres en minusculas
        /// </summary>
        /// <param name="cadena">cadena de caracteres que ingresa el usuario</param>
        /// <returns></returns>
        internal bool MinusculaCadena(string cadena)
        {
            string cadenaMinuscula = cadena.ToLower();
            if (VerificarNumero(cadena))
            {
                return false;
            }
            else
            {
                if (cadena.Equals(cadenaMinuscula))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// verifica si una cadena esta formada de numeros
        /// </summary>
        /// <param name="cadena">cadena ingresada por el usuario</param>
        /// <returns></returns>
        internal bool VerificarNumero(string cadena)
        {
            int i = 0;
            bool result = int.TryParse(cadena, out i);
            if (result)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Determina si una cadena es valida
        /// </summary>
        /// <param name="cadena">cadena digitada por el usuario</param>
        /// <param name="caracter">caracter usado para las condiciones de si es valida la cadena</param>
        /// <returns></returns>
        internal string CadenaValida(string cadena, string caracter)
        {
            switch (caracter)
            {

                case "A":
                    if (MayusculaCadena(cadena))
                    {
                        return "Es válida";
                    }
                    else
                    {
                        return "No es válida";
                    }
                case "a":

                    if (MinusculaCadena(cadena))
                    {
                        return "Es válida";
                    }
                    else
                    {
                        return "No es válida";
                    }

                case "9":
                    if (VerificarNumero(cadena))
                    {
                        return "Es válida";
                    }
                    else
                    {
                        return "No es válida";
                    }

                case "Z":
                    return "Es válida";


                default:
                    return "No funciona ese carácter";

            }
        }


        internal bool CompararCadena(string cadena1, string cadena2)
        {
            bool esVerdadero = false;

            if (cadena1.Length > cadena2.Length || cadena1.Length < cadena2.Length)
            {
                return esVerdadero;
            }

            for (int i = 0; i < cadena1.Length; i++)
            {
                if (cadena1[i] == cadena2[i])
                {
                    esVerdadero = true;
                }
                else
                {
                    esVerdadero = false;
                    return esVerdadero;
                }
            }

            return esVerdadero;
        }
    }
}


