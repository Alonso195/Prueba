﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2
{
    class ArregloUtilidades
    {
        static int[,] matriz = new int[3, 3];

        /// <summary>
        /// Este metodo ordena el arreglo
        /// </summary>
        /// <returns>retorna un string con el arreglo ordenado</returns>
        internal string OrdenarArreglo()
        {
            int[] array = { 8, 10, 20, 6, 5, 85, 42, 1000 };
            Array.Sort(array);
            string arrayText = "";

            for (int i = 0; i < array.Length; i++)
            {
                arrayText += String.Format(array[i].ToString() + ",");
            }

            return arrayText;
        }

        /// <summary>
        /// Este metodo obtiene el rango del arrglo restanto el menor del mayor 
        /// </summary>
        /// <returns>rango</returns>
        internal string RangoNumeros()
        {
            int[] array = { 8, 10, 20, 6, 5, 85, 42, 1000 };
            Array.Sort(array);

            int menor = array[0];
            int mayor = array[array.Length - 1];
            int rango = mayor - menor;
            return rango.ToString() + "\n" + OrdenarArreglo();

        }

        internal string Promedio()
        {
            int sumatoria = 0;
            int[] array = { 8, 10, 20, 6, 5, 85, 42, 1000 };

            for (int i = 0; i < array.Length; i++)
            {
                sumatoria += array[i];
            }
            sumatoria /= array.Length;

            return sumatoria.ToString();
        }

        internal string MayorMenor()
        {
            int[] array = { 8, 10, 20, 6, 5, 85, 42, 1000 };
            Array.Sort(array);

            int menor = array[0];
            int mayor = array[array.Length - 1];

            return "El menor es " + menor.ToString() + "\n" +
                    "El mayor es " + mayor.ToString()
                    + "\n" + OrdenarArreglo();

        }

        internal void LlenarMatriz()
        {
            Random rnd = new Random();
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    matriz[i, j] = rnd.Next(0, 2);
                }
            }


        }

        internal string ImprimirMatriz()
        {
            string matrizString = "";

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    matrizString += matriz[i, j].ToString() + " ";
                }
                matrizString += "\n";
            }
            return matrizString;
        }
    }
}
