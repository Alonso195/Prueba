﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2
{
    class NumeroUtilidades
    {
        /// <summary>
        /// Este metodo verifica si un número es narcisista o no
        /// </summary>
        /// <param name="num">número entero ingresado por el usuario</param>
        /// <returns>Retorna un string explicando si el numero es narcisista o no</returns>
        internal string NumeroNarcisista(int num)
        {

            string num6 = num.ToString();
            double sumatoria = 0;

            for (int i = 0; i < num6.Length; i++)
            {
                sumatoria += Math.Pow(Convert.ToDouble(num6[i].ToString()), num6.Length);
            }
            if (num == sumatoria)
            {
                return "Es un número narcisista";
            }

            return "No es un número narcisista";

        }


        internal int RaizAproximada(int num)
        {
            for (int i = 0; i < num; i++)
            {
                int cuadrado = i * i;
                if (cuadrado > num)
                {
                    return i - 1;
                }
            }
            return 0;
        }


        internal string IntercambioValores(int a, int b)
        {
            int temp = b;
            b = a;
            a = temp;

            return String.Format("El valor de a ahora es {0} \n El valor de b ahora es {1}",a,b);
        }

        internal int Invertir(int numero)
        {
            int inv = 0;
            while (numero != 0)
            {
                int ult = numero % 10;
                inv = inv * 10 + ult;
                numero /= 10;
            }
            return inv;
        }
    }
}
