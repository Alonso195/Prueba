﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quiz2
{
    public partial class ArregloInterfaz : Form
    {
        ArregloUtilidades arregloUtilidades;
        public ArregloInterfaz()
        {
            InitializeComponent();
        }
        private void ArregloInterfaz_Load(object sender, EventArgs e)
        {
            arregloUtilidades = new ArregloUtilidades();
        }
        /// <summary>
        /// llama al metodo OrdenarArreglo en la clase ArregloUtilidades
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        private void OrdenarArreglo(object sender, EventArgs e)
        {
            
            rtxtMostrarArreglo.Text = arregloUtilidades.OrdenarArreglo();
        }


        // <summary>
        /// llama al metodo RangoNumeros en la clase ArregloUtilidades
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObtenerRangoButtonClick(object sender, EventArgs e)
        {
            rangoTextBox.Text = arregloUtilidades.RangoNumeros();
        }




        // <summary>
        /// llama al metodo Ordenar arreglo en la clase ArregloUtilidades
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PromedioButtonClick(object sender, EventArgs e)
        {
            DialogResult result = System.Windows.Forms.MessageBox.Show(arregloUtilidades.Promedio(),
              "Promedio", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "El promedio es " + arregloUtilidades.Promedio()+"\n"+
                arregloUtilidades.MayorMenor();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            arregloUtilidades.LlenarMatriz();
            richTextBox2.Text = arregloUtilidades.ImprimirMatriz();
        }
    }
}
