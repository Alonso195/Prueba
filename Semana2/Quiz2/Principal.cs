﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quiz2
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

       

        private void MostrarArreglo(object sender, EventArgs e)
        {
            ArregloInterfaz arreglo = new ArregloInterfaz();
            arreglo.Show();
        }

        private void MostrarNumero(object sender, EventArgs e)
        {
            NumeroInterfaz numero = new NumeroInterfaz();
            numero.Show();
        }

        private void MostrarTexto(object sender, EventArgs e)
        {
            TextoInterfaz texto = new TextoInterfaz();
            texto.Show();
        }
    }
}
