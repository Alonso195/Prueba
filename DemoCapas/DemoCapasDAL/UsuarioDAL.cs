﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoCapasENL;
using Npgsql;
namespace DemoCapasDAL
{
    public class UsuarioDAL
    {
        public List<EUsuario> CargarTodo()
        {
            // se conecta a la base de datos
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                List<EUsuario> usuarios = new List<EUsuario>();
                con.Open();
                string sql = @"select id, cedula, usuario, nombre, apellido_uno, apellido_dos, pass, email from usuario";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    usuarios.Add(CargarUsuario(reader));
                    //usuarios += reader.GetInt32(0) + "\t";
                    //usuarios += reader.GetString(1) + "\t";
                    //usuarios += reader.GetString(2) + "\n";
                }
                return usuarios;
            }

        }

        private EUsuario CargarUsuario(NpgsqlDataReader reader)
        {
            EUsuario usu = new EUsuario();
            //No es necesario validar el id es solo para ejemplo
            usu.Id = reader["id"] != DBNull.Value ? Convert.ToInt32(reader["id"]):0;
            usu.Cedula = Convert.ToString(reader["cedula"]);
            usu.Usuario = Convert.ToString(reader["usuario"]);

            usu.Nombre = Convert.ToString(reader["nombre"]);
            usu.ApeUno = Convert.ToString(reader["apellido_uno"]);
            usu.ApeDos = Convert.ToString(reader["apellido_dos"]);
            usu.Password = Convert.ToString(reader["pass"]);
            usu.Email = Convert.ToString(reader["email"]);







            return usu;
        }
    }
}
