﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoCapasENL
{
    public class EUsuario
    {
        public int Id { get; set; }
        public string Cedula { get; set; }
        public string Usuario { get; set; }
        public string Nombre { get; set; }
        public string ApeUno { get; set; }
        public string ApeDos { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }



    }
}
