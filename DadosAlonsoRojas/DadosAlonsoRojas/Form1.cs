﻿using DadosBOL;
using DadosENL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DadosAlonsoRojas
{
    public partial class Form1 : Form
    {
        Image[] arrayImage;
        //int i = 0;
        
        UsuarioBOL bol;
        Usuario usu;
        static int ValorDado1 = 0;
        static int ValorDado2 = 0;
        private int contTimer = 0;


        public Form1()
        {
            arrayImage = new Image[6];
            InitializeComponent();
            CargarImagenes();
            MostrarDados();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            bol = new UsuarioBOL();
            usu = new Usuario();
        }

        public void CargarImagenes()
        {
            arrayImage[0] = Properties.Resources._1;
            arrayImage[1] = Properties.Resources._2;
            arrayImage[2] = Properties.Resources._3;
            arrayImage[3] = Properties.Resources._4;
            arrayImage[4] = Properties.Resources._5;
            arrayImage[5] = Properties.Resources._6;

        }

        public void MostrarDados()
        {
            for (int i = 0; i < arrayImage.Length; i++)
            {
                pictureBox1.Image = arrayImage[i];
                pictureBox2.Image = arrayImage[i];
            }
        }

        private void TirarDado_Click(object sender, EventArgs e)
        {
            timer1.Start();

        }

        private void CalcularPartida()
        {
            int resultEsperado = Convert.ToInt32(txtResultEsperado.Text);
            int resultado = ValorDado1 + ValorDado2;
            if (resultado == resultEsperado)
            {
                
            }
            if (resultado - resultEsperado == 1
                || resultado - resultEsperado == -1)
            {
                
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            contTimer++;
            int i = 0;
            while (i <= arrayImage.Length)
            {
                Random random = new Random();
                int dado1 = random.Next(1, 6);
                int dado2 = random.Next(1, 6);
                ValorDado1 = dado1 + 1;
                ValorDado2 = dado2 + 1;
                pictureBox1.Image = arrayImage[dado1];
                pictureBox2.Image = arrayImage[dado2];

                i++;
            }
            if (contTimer == 10)
            {
                CalcularPartida();
                timer1.Stop();
                i = 0;
                contTimer = 0;
            }




        }

        private void NuevaPartida_Click(object sender, EventArgs e)
        {
            try
            {
                usu.NombreUsu = txtNombreUsu.Text.Trim();
            }
            catch (Exception)
            {

                throw;
            }
        }


    }
}
