﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WebService.BCCR;

namespace WebService
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnTest_Click(object sender, EventArgs e)
        {
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("dd/MM/yyyy");

            wsIndicadoresEconomicosSoapClient ws =
                new wsIndicadoresEconomicosSoapClient("wsIndicadoresEconomicosSoap");



            string Venta = ws.ObtenerIndicadoresEconomicosXML("317", fecha, fecha, "Alonso-UTN", "N");
            //txtResultado.Text = rest;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(Venta);

            XmlNodeList list = doc.GetElementsByTagName("NUM_VALOR");

            double tipoCambio = Convert.ToDouble(list[0].InnerText);
            txtResultado.Text = tipoCambio.ToString();
        }
    }
}
