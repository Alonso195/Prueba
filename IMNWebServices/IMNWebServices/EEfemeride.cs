﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMNWebServices
{
    class EEfemeride
    {
    }


    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class EFEMERIDES
    {

        private EFEMERIDESEFEMERIDE_SOL eFEMERIDE_SOLField;

        private EFEMERIDESEFEMERIDE_LUNA eFEMERIDE_LUNAField;

        private EFEMERIDESFASELUNAR fASELUNARField;

        /// <remarks/>
        public EFEMERIDESEFEMERIDE_SOL EFEMERIDE_SOL
        {
            get
            {
                return this.eFEMERIDE_SOLField;
            }
            set
            {
                this.eFEMERIDE_SOLField = value;
            }
        }

        /// <remarks/>
        public EFEMERIDESEFEMERIDE_LUNA EFEMERIDE_LUNA
        {
            get
            {
                return this.eFEMERIDE_LUNAField;
            }
            set
            {
                this.eFEMERIDE_LUNAField = value;
            }
        }

        /// <remarks/>
        public EFEMERIDESFASELUNAR FASELUNAR
        {
            get
            {
                return this.fASELUNARField;
            }
            set
            {
                this.fASELUNARField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class EFEMERIDESEFEMERIDE_SOL
    {

        private string sALEField;

        private string sEPONEField;

        /// <remarks/>
        public string SALE
        {
            get
            {
                return this.sALEField;
            }
            set
            {
                this.sALEField = value;
            }
        }

        /// <remarks/>
        public string SEPONE
        {
            get
            {
                return this.sEPONEField;
            }
            set
            {
                this.sEPONEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class EFEMERIDESEFEMERIDE_LUNA
    {

        private string sALEField;

        private string sEPONEField;

        /// <remarks/>
        public string SALE
        {
            get
            {
                return this.sALEField;
            }
            set
            {
                this.sALEField = value;
            }
        }

        /// <remarks/>
        public string SEPONE
        {
            get
            {
                return this.sEPONEField;
            }
            set
            {
                this.sEPONEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class EFEMERIDESFASELUNAR
    {

        private byte idField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }


}
