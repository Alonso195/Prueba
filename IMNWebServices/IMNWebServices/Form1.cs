﻿using IMNWebServices.IMN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMNWebServices
{
    public partial class Form1 : Form
    {
        WSMeteorologicoClient ws;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ws = new WSMeteorologicoClient("WSMeteorologico");
            GetEfemerides();
            GetRegiones();
            GetCiudades();
        }
        /// <summary>
        /// Obtiene las efemerides del servicio y los carga a los txt
        /// </summary>
        private void GetEfemerides()
        {
            
            string efeme = ws.efemerides(new efemerides());

            EFEMERIDES res = efeme.ParseXML<EFEMERIDES>();
            string salidaSol = res.EFEMERIDE_SOL.SALE;
            string puetaSol = res.EFEMERIDE_SOL.SEPONE;
            string salidaLuna = res.EFEMERIDE_LUNA.SALE;
            string puestaLuna = res.EFEMERIDE_LUNA.SEPONE;
            string faseLunar = res.FASELUNAR.Value;

            txtFaseL.Text = faseLunar;
            txtPuestaL.Text = puestaLuna;
            txtSalidaL.Text = salidaLuna;
            txtSalidaS.Text = salidaSol;
            txtPuestaS.Text = puetaSol;
        }
        private void GetRegiones()
        {
            string regiones = ws.pronosticoRegional(new pronosticoRegion());
            PRONOSTICO_REGIONAL res = regiones.ParseXML<PRONOSTICO_REGIONAL>();
            PRONOSTICO_REGIONALREGION[] list = res.REGIONES;
            cmbRegiones.DataSource = list;
            Console.WriteLine(regiones);
        }

        private void GetCiudades()
        {
            string regiones = ws.pronosticoPorCiudad(new pronosticoCiudad());
            Console.WriteLine(regiones);
            PRONOSTICO_PORCIUDADES res = regiones.ParseXML<PRONOSTICO_PORCIUDADES>();
            PRONOSTICO_PORCIUDADESCIUDAD[] list = res.CIUDADES;
            comboBox1.DataSource = list;
        }


        private void GetPronosticosRegional()
        {
            PRONOSTICO_REGIONALREGION select = (PRONOSTICO_REGIONALREGION)cmbRegiones.SelectedItem;
            richTextBox1.Text = "Madrugada: "+select.COMENTARIOMADRUGADA+"\n"+
                "Mañana: " + select.COMENTARIOMANANA + "\n"+
                "Tarde: " + select.COMENTARIOTARDE + "\n"+
                "Noche: " + select.COMENTARIONOCHE + "\n";

           
            pictureBoxRegional.ImageLocation = "https://www.imn.ac.cr/" + select.ESTADOMADRUGADA.imgPath;
            pictureBox1.ImageLocation = "https://www.imn.ac.cr/" + select.ESTADOMANANA.imgPath;
            pictureBox2.ImageLocation = "https://www.imn.ac.cr/" + select.ESTADOTARDE.imgPath;
            pictureBox3.ImageLocation = "https://www.imn.ac.cr/" + select.ESTADONOCHE.imgPath;

        }

        private void GetPronosticoReg_Click(object sender, EventArgs e)
        {
            GetPronosticosRegional();
        }

        private void GetPronosticosCiudad()
        {
            PRONOSTICO_PORCIUDADESCIUDAD select = (PRONOSTICO_PORCIUDADESCIUDAD)comboBox1.SelectedItem;
            richTextBox2.Text = "Madrugada: " + select.COMENTARIOMADRUGADA + "\n" +
                "Mañana: " + select.COMENTARIOMANANA + "\n" +
                "Tarde: " + select.COMENTARIOTARDE + "\n" +
                "Noche: " + select.COMENTARIONOCHE + "\n";
            string urlImage = 
            pictureBoxTarde.ImageLocation = "https://www.imn.ac.cr/" + select.ESTADOTARDE.imgPath;
            pictureBoxMadrugada.ImageLocation = "https://www.imn.ac.cr/" + select.ESTADOMADRUGADA.imgPath;
            pictureBoxNoche.ImageLocation = "https://www.imn.ac.cr/" + select.ESTADONOCHE.imgPath;
            pictureBoxManana.ImageLocation = "https://www.imn.ac.cr/" + select.ESTADOMANANA.imgPath;
        }

        private void GetPronosticoCiudad_Click(object sender, EventArgs e)
        {
            GetPronosticosCiudad();
        }
    }
}
