﻿namespace IMNWebServices
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtFaseL = new System.Windows.Forms.TextBox();
            this.txtPuestaL = new System.Windows.Forms.TextBox();
            this.txtSalidaL = new System.Windows.Forms.TextBox();
            this.txtPuestaS = new System.Windows.Forms.TextBox();
            this.txtSalidaS = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBoxRegional = new System.Windows.Forms.PictureBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbRegiones = new System.Windows.Forms.ComboBox();
            this.rEGIONESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pRONOSTICOREGIONALBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pictureBoxManana = new System.Windows.Forms.PictureBox();
            this.pictureBoxMadrugada = new System.Windows.Forms.PictureBox();
            this.pictureBoxNoche = new System.Windows.Forms.PictureBox();
            this.pictureBoxTarde = new System.Windows.Forms.PictureBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.cIUDADESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pRONOSTICOPORCIUDADESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRegional)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEGIONESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALBindingSource)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxManana)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMadrugada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNoche)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTarde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIUDADESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOPORCIUDADESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(1, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1024, 663);
            this.tabControl1.TabIndex = 10;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabPage1.Controls.Add(this.txtFaseL);
            this.tabPage1.Controls.Add(this.txtPuestaL);
            this.tabPage1.Controls.Add(this.txtSalidaL);
            this.tabPage1.Controls.Add(this.txtPuestaS);
            this.tabPage1.Controls.Add(this.txtSalidaS);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1016, 634);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Efemeride";
            // 
            // txtFaseL
            // 
            this.txtFaseL.Enabled = false;
            this.txtFaseL.Font = new System.Drawing.Font("MS Reference Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFaseL.Location = new System.Drawing.Point(418, 431);
            this.txtFaseL.Name = "txtFaseL";
            this.txtFaseL.Size = new System.Drawing.Size(500, 40);
            this.txtFaseL.TabIndex = 19;
            // 
            // txtPuestaL
            // 
            this.txtPuestaL.Enabled = false;
            this.txtPuestaL.Font = new System.Drawing.Font("MS Reference Sans Serif", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPuestaL.Location = new System.Drawing.Point(418, 329);
            this.txtPuestaL.Name = "txtPuestaL";
            this.txtPuestaL.Size = new System.Drawing.Size(269, 52);
            this.txtPuestaL.TabIndex = 18;
            // 
            // txtSalidaL
            // 
            this.txtSalidaL.Enabled = false;
            this.txtSalidaL.Font = new System.Drawing.Font("MS Reference Sans Serif", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalidaL.Location = new System.Drawing.Point(418, 258);
            this.txtSalidaL.Name = "txtSalidaL";
            this.txtSalidaL.Size = new System.Drawing.Size(269, 52);
            this.txtSalidaL.TabIndex = 17;
            // 
            // txtPuestaS
            // 
            this.txtPuestaS.Enabled = false;
            this.txtPuestaS.Font = new System.Drawing.Font("MS Reference Sans Serif", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPuestaS.Location = new System.Drawing.Point(418, 167);
            this.txtPuestaS.Name = "txtPuestaS";
            this.txtPuestaS.Size = new System.Drawing.Size(269, 52);
            this.txtPuestaS.TabIndex = 16;
            // 
            // txtSalidaS
            // 
            this.txtSalidaS.Enabled = false;
            this.txtSalidaS.Font = new System.Drawing.Font("MS Reference Sans Serif", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalidaS.Location = new System.Drawing.Point(418, 93);
            this.txtSalidaS.Name = "txtSalidaS";
            this.txtSalidaS.Size = new System.Drawing.Size(269, 52);
            this.txtSalidaS.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(163, 441);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(209, 40);
            this.label5.TabIndex = 14;
            this.label5.Text = "Fase Lunar";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(163, 268);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(222, 40);
            this.label4.TabIndex = 13;
            this.label4.Text = "Salida Luna";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(163, 337);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(231, 40);
            this.label3.TabIndex = 12;
            this.label3.Text = "Puesta Luna";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(163, 167);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 40);
            this.label2.TabIndex = 11;
            this.label2.Text = "Puesta Sol";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(163, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 40);
            this.label1.TabIndex = 10;
            this.label1.Text = "Salida Sol";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pictureBox3);
            this.tabPage2.Controls.Add(this.pictureBox2);
            this.tabPage2.Controls.Add(this.pictureBox1);
            this.tabPage2.Controls.Add(this.pictureBoxRegional);
            this.tabPage2.Controls.Add(this.richTextBox1);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.cmbRegiones);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1016, 634);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Pronóstico Regional";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureBoxRegional
            // 
            this.pictureBoxRegional.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxRegional.Location = new System.Drawing.Point(816, 27);
            this.pictureBoxRegional.Name = "pictureBoxRegional";
            this.pictureBoxRegional.Size = new System.Drawing.Size(152, 117);
            this.pictureBoxRegional.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRegional.TabIndex = 4;
            this.pictureBoxRegional.TabStop = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("MS Reference Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(32, 101);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(652, 205);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(64, 369);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 51);
            this.button1.TabIndex = 1;
            this.button1.Text = "Buscar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.GetPronosticoReg_Click);
            // 
            // cmbRegiones
            // 
            this.cmbRegiones.DataSource = this.rEGIONESBindingSource;
            this.cmbRegiones.DisplayMember = "nombre";
            this.cmbRegiones.Font = new System.Drawing.Font("MS Reference Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbRegiones.FormattingEnabled = true;
            this.cmbRegiones.Location = new System.Drawing.Point(32, 27);
            this.cmbRegiones.Name = "cmbRegiones";
            this.cmbRegiones.Size = new System.Drawing.Size(288, 42);
            this.cmbRegiones.TabIndex = 0;
            // 
            // rEGIONESBindingSource
            // 
            this.rEGIONESBindingSource.DataMember = "REGIONES";
            this.rEGIONESBindingSource.DataSource = this.pRONOSTICOREGIONALBindingSource;
            // 
            // pRONOSTICOREGIONALBindingSource
            // 
            this.pRONOSTICOREGIONALBindingSource.DataSource = typeof(IMNWebServices.PRONOSTICO_REGIONAL);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.pictureBoxManana);
            this.tabPage3.Controls.Add(this.pictureBoxMadrugada);
            this.tabPage3.Controls.Add(this.pictureBoxNoche);
            this.tabPage3.Controls.Add(this.pictureBoxTarde);
            this.tabPage3.Controls.Add(this.richTextBox2);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.comboBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1016, 634);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Pronóstico Ciudades";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // pictureBoxManana
            // 
            this.pictureBoxManana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxManana.Location = new System.Drawing.Point(783, 461);
            this.pictureBoxManana.Name = "pictureBoxManana";
            this.pictureBoxManana.Size = new System.Drawing.Size(174, 114);
            this.pictureBoxManana.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxManana.TabIndex = 9;
            this.pictureBoxManana.TabStop = false;
            // 
            // pictureBoxMadrugada
            // 
            this.pictureBoxMadrugada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxMadrugada.Location = new System.Drawing.Point(783, 317);
            this.pictureBoxMadrugada.Name = "pictureBoxMadrugada";
            this.pictureBoxMadrugada.Size = new System.Drawing.Size(174, 114);
            this.pictureBoxMadrugada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxMadrugada.TabIndex = 8;
            this.pictureBoxMadrugada.TabStop = false;
            // 
            // pictureBoxNoche
            // 
            this.pictureBoxNoche.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxNoche.Location = new System.Drawing.Point(783, 174);
            this.pictureBoxNoche.Name = "pictureBoxNoche";
            this.pictureBoxNoche.Size = new System.Drawing.Size(174, 114);
            this.pictureBoxNoche.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxNoche.TabIndex = 7;
            this.pictureBoxNoche.TabStop = false;
            // 
            // pictureBoxTarde
            // 
            this.pictureBoxTarde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxTarde.Location = new System.Drawing.Point(783, 27);
            this.pictureBoxTarde.Name = "pictureBoxTarde";
            this.pictureBoxTarde.Size = new System.Drawing.Size(174, 114);
            this.pictureBoxTarde.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxTarde.TabIndex = 6;
            this.pictureBoxTarde.TabStop = false;
            // 
            // richTextBox2
            // 
            this.richTextBox2.Font = new System.Drawing.Font("MS Reference Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.Location = new System.Drawing.Point(35, 101);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(659, 317);
            this.richTextBox2.TabIndex = 5;
            this.richTextBox2.Text = "";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(35, 441);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(116, 51);
            this.button2.TabIndex = 4;
            this.button2.Text = "Buscar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.GetPronosticoCiudad_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.cIUDADESBindingSource;
            this.comboBox1.DisplayMember = "nombre";
            this.comboBox1.Font = new System.Drawing.Font("MS Reference Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(35, 27);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(288, 42);
            this.comboBox1.TabIndex = 3;
            // 
            // cIUDADESBindingSource
            // 
            this.cIUDADESBindingSource.DataMember = "CIUDADES";
            this.cIUDADESBindingSource.DataSource = this.pRONOSTICOPORCIUDADESBindingSource;
            // 
            // pRONOSTICOPORCIUDADESBindingSource
            // 
            this.pRONOSTICOPORCIUDADESBindingSource.DataSource = typeof(IMNWebServices.PRONOSTICO_PORCIUDADES);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(816, 179);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(152, 117);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(816, 334);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(152, 117);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Location = new System.Drawing.Point(816, 484);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(152, 117);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 7;
            this.pictureBox3.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 664);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRegional)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rEGIONESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOREGIONALBindingSource)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxManana)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMadrugada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNoche)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTarde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIUDADESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRONOSTICOPORCIUDADESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txtFaseL;
        private System.Windows.Forms.TextBox txtPuestaL;
        private System.Windows.Forms.TextBox txtSalidaL;
        private System.Windows.Forms.TextBox txtPuestaS;
        private System.Windows.Forms.TextBox txtSalidaS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cmbRegiones;
        private System.Windows.Forms.BindingSource rEGIONESBindingSource;
        private System.Windows.Forms.BindingSource pRONOSTICOREGIONALBindingSource;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.BindingSource pRONOSTICOPORCIUDADESBindingSource;
        private System.Windows.Forms.BindingSource cIUDADESBindingSource;
        private System.Windows.Forms.PictureBox pictureBoxRegional;
        private System.Windows.Forms.PictureBox pictureBoxTarde;
        private System.Windows.Forms.PictureBox pictureBoxMadrugada;
        private System.Windows.Forms.PictureBox pictureBoxNoche;
        private System.Windows.Forms.PictureBox pictureBoxManana;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

